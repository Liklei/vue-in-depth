/** 
 * @func 绘制样式颜色阴影
*/
var cc = document.getElementById('myCanvas');
var ctx;

//TODO: 支持性检测
if (cc.getContext('2d')) {
    ctx = cc.getContext('2d');
}else{
   console.log("浏览器不支持");
}

// TODO: 色彩样式  fillStyle = color 图形填充样色； strokeStyle = color 轮廓颜色

for(var i = 1; i < 6; i++) {
    for (var j = 1; j < 6; j++) {
        ctx.fillStyle = 'rgb(' + Math.floor(255 - 42.5 * i) + ',' + Math.floor(255 - 42.5 * j) + ',0)'; // 
        ctx.fillRect(j * 25, i * 25, 25, 25);
    }
}

// TODO: 线型的样式  

// 设置线条宽度。
// lineWidth = value

// 设置线条末端样式。
// lineCap = type
// 有三个值：butt 线段末端以方形结束。 round 线段末端以圆形结束。 square 线段末端以方形结束， 但是增加了一个宽度和线段相同， 高度是线段厚度一半的矩形区域。

// 设定线条与线条间接合处的样式。
// lineJoin = type
// 此属性有3个值： round, bevel and miter。 默认值是 miter。 注意： 如果2个相连部分在同一方向， 那么lineJoin不会产生任何效果， 因为在那种情况下不会出现连接区域。 round 通过填充一个额外的， 圆心在相连部分末端的扇形， 绘制拐角的形状。 圆角的半径是线段的宽度。 bevel 在相连部分的末端填充一个额外的以三角形为底的区域， 每个部分都有各自独立的矩形拐角。 miter 通过延伸相连部分的外边缘， 使其相交于一点， 形成一个额外的菱形区域。 这个设置可以通过 miterLimit 属性看到效果

// 限制当两条线相交时交接处最大长度； 所谓交接处长度（ 斜接长度） 是指线条交接处内角顶点到外角顶点的长度。
// miterLimit = value

// 返回一个包含当前虚线样式， 长度为非负偶数的数组。
// getLineDash()

// 设置当前虚线样式。
// setLineDash(segments)

// 设置虚线样式的起始偏移量。
// lineDashOffset = value。


// TODO: 渐变Gradients

//线性渐变
var linearGradient = ctx.createLinearGradient(50, 50, 250, 250); // 表示渐变的起点 (x1,y1) 与终点 (x2,y2)
linearGradient.addColorStop(0, 'yellow'); // position 参数必须是一个 0.0 与 1.0 之间的数值; 颜色值
linearGradient.addColorStop(.5, 'red');
linearGradient.addColorStop(1, 'green');
ctx.fillStyle = linearGradient;
ctx.fillRect(50, 50, 200, 200);

//径向渐变

var rr = ctx.createRadialGradient(100, 100, 40, 100, 100, 90);
rr.addColorStop(0, 'red');
rr.addColorStop(.5, 'yellow');
rr.addColorStop(1, 'blue');
ctx.fillStyle = rr;
ctx.fillRect(20, 20, 160, 160)
ctx.fill();

// TODO： 图案样式 Patterns  createPattern(image, type) 该方法接受两个参数。Image 可以是一个 Image 对象的引用，或者另一个 canvas 对象。Type 必须是下面的字符串值之一：repeat，repeat-x，repeat-y 和 no-repeat。
var pngs = new Image();
pngs.src = './test.png';
pngs.onload = function() {
    var png = ctx.createPattern(pngs, 'repeat');
    ctx.fillStyle = png;
    ctx.fillRect(300, 150, 60, 60);
};




// TODO: 阴影

var img = new Image();
img.src='./img.jpg';
img.onload = function() {
    ctx.shadowOffsetX = 10; // 负值向左偏移
    ctx.shadowOffsetY = 10; // 负值向上偏移
    ctx.shadowBlur = 8;
    ctx.shadowColor='pink';
    ctx.drawImage(img, 250, 250, 400, 300);
};

// TODO: 填充规则 当使用fill时，可以选用一个填充规则（"nonzero": 默认值. "evenodd"）




















