
/** 
 * @func 绘制基础形状
 */
var cc = document.getElementById('myCanvas');
var ctx;

//TODO: 支持性检测
if (cc.getContext('2d')) {
    ctx = cc.getContext('2d');
}else{
   console.log("浏览器不支持");
}

//TODO：绘制矩形x, y 相对画布的位置，w，h宽高
ctx.strokeRect(20, 20, 100, 100); // 绘制矩形边框
ctx.fillRect(40, 40, 50, 50); //填充矩形
ctx.clearRect(50, 50, 30, 30); //清除指定矩形区域，使其全透明

//TODO: 绘制路径：路径是通过不同颜色和宽度的线段或曲线相连形成的不同形状的点的集合。一个路径，甚至一个子路径，都是闭合的。

ctx.beginPath(); //新建路径命令
ctx.moveTo(150, 150); // 将笔触移动到指定的坐标x以及y上
ctx.lineTo(150, 200); // 绘制一条从当前位置到指定x以及y位置的直线
ctx.lineTo(200, 200);
ctx.closePath(); // 闭合路径之后图形绘制命令又重新指向到上下文中
ctx.stroke(); // 通过线条来绘制图形轮廓

ctx.beginPath();
ctx.moveTo(300, 300);
ctx.lineTo(300, 400);
ctx.lineTo(400, 400);
ctx.fill(); // 通过填充路径的内容区域生成实心的图形
ctx.closePath();

//TODO: 绘制弧线：arc(x, y, radius, startAngle, endAngle, anticlockwise), 画一个以（x,y）为圆心的以radius为半径的圆弧（圆），从startAngle开始到endAngle结束，参数anticlockwise 为一个布尔值。为true时，是逆时针方向，否则顺时针方向。
ctx.beginPath();
ctx.arc(200, 70, 50, 0, Math.PI / 2, true);
ctx.stroke();

ctx.beginPath();
ctx.arc(400, 70, 50, 0, Math.PI / 2, false);
ctx.fill();

//TODO: 绘制矩形：rect(x, y, width, height)

//TODO: 绘制矩形：rect(x, y, width, height)

// TODO: Path2D--Path2D对象已可以在较新版本的浏览器中使用，用来缓存或记录绘画命令，这样你将能快速地回顾路径。Path2D() Path2D() 会返回一个新初始化的Path2D对象（ 可能将某一个路径作为变量—— 创建一个它的副本， 或者将一个包含SVG path数据的字符串作为变量）

//new Path2D(); // 空的Path对象 
//new Path2D(path); // 克隆Path对象 
//new Path2D(d); // 从SVG建立Path对象著作权归作者所有。

var path = new Path2D();
path.rect(120, 120, 50, 50);
ctx.stroke(path);















