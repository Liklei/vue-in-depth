const http = require('http');
const https = require('https');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');
const mongoose = require('mongoose');
const config = require('./config');

const app = new Koa();

const product = require('./routes/product');
const user = require('./routes/user');

//连接数据库
mongoose.connect(config.db, {useNewUrlParser: true,  useFindAndModify: false}, (err) => {
    if (err) {
        console.error('Failed to connect to database');
    } else {
        console.log('Connecting database successfully');
    }
});

// 处理跨域相关
app.use(cors({
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization', 'Date'],
    maxAge: 25000,
    credentials:  true,
    allowMethods: ['GET', 'POST', 'OPTIONS', 'DELETE', 'HEAD', 'PUT'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Custom-Header', 'anonymous']
}))

// 解析request的body
app.use(bodyParser());

//加载路由
app.use(product.routes()).use(product.allowedMethods());
app.use(user.routes()).use(user.allowedMethods());

// 启动服务
app.listen(config.HTTP_SERVE_PORT)