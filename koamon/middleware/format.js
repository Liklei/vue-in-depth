/** 
 * @func mdw-中间件
 * @author Lance
*/
let crypto;

try {
    crypto = require('crypto');
} catch (err) {
    console.log('不支持 crypto!');
}

class Format{
   constructor() {
   }
   getToken(content) {
        let md5 = crypto.createHash('md5');
        md5.update(content);
        return md5.digest('hex');
   }
   checkPhone(str) {
        return /^1((3[\d])|(4[5,6,7,9])|(5[0-3,5-9])|(6[5-7])|(7[0-8])|(8[\d])|(9[1,8,9]))\d{8}$/.test(str)? true : false;
   }
   checkPassWord(str) {
        return str.length === 6 ? true : false;
   }
}
module.exports = new Format();