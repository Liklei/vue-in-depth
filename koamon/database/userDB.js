/** 
 * @function  user-database
 * @author lance
*/
// https://segmentfault.com/a/1190000008245062#articleHeader15

const mongoose = require('mongoose');

// 账户的数据库模型
const UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String
});

const User = mongoose.model('User', UserSchema);

// user db option
class UserDB{
   constructor() {
   }
   // 查询
   query(data={}) {
      return new Promise((resolve, reject) => {
        User.find(data, (err, res)=>{
            if (err) reject(err)
            resolve(res);
        });
      });
   }
   queryLimit(key) {
      return new Promise((resolve, reject) => {
        User.findOne(key, (err, res)=>{
            if (err) reject(err);
            resolve(res);
        });
      });
   }
   // 新增数据 [insertMany: ([], func)]
   save(data) {
      return new Promise((resolve, reject) => {
          User.create(data, (err, res) => {
              if (err) reject(err)
              resolve(res)
          });
      });
   }
   // 删除数据
   delete(key) {
      return new Promise((resolve, reject) => {
          User.findOneAndRemove(key, (err, res) => {
              if (err) reject(err);
              resolve(res);
          });
      });
   }
   // 修改数据
   change(targetData, reData) {
      return new Promise((resolve, reject) => {
          User.update(targetData, reData, {multi: true}, (err, res) => {
              if (err) reject(err);
              resolve(res);
          });
      });
   }
}
module.exports = new UserDB()