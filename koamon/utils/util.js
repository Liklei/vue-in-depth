/** 
 * @function util-tool
 * @author lance
*/

function checkObjectType(data) {
    let type = Object.prototype.toString.call(data)
    switch (type) {
        case "[object Array]":
          return 'Array'
        break;    
        case "[object String]":
        return 'String'
        break;
        case "[object Number]":
        return 'Number'
        break;
        case "[object Function]":
        return 'Function'
        break;
        default:
        return 'unKnow'
    }
}

function checkObjectLength(data) {
     return Object.entries(data).length
}

module.exports ={
    checkObjectType,
    checkObjectLength

}