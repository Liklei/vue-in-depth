/** 
 * @function 安装babel后支持import
 * @author Lance
*/
require('babel-register')
    ({
        plugins: ['babel-plugin-transform-es2015-modules-commonjs'],
    })

module.exports = require('./app.js')