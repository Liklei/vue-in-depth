/** 
 * @function normal-config 
 * @author Lance
*/
module.exports = {
  HTTP_SERVE_PORT: 8081,
  HTTPS_SERVE_PORT: 8082,
  db: "mongodb://localhost/testDB"
}