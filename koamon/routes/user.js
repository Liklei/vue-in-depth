/** 
 * @func user-route
 * @author Lance
*/
const Router = require('koa-router');
const USER_DB = require('../database/userDB');
const util = require('../utils/util');

// 路由前缀
const router = new Router({
    prefix: "/api"
});

router.post('/user', async (ctx, next) => {
    let result = {
        code: 0,
        data: null,
    };
    if (util.checkObjectLength(ctx.request.query) == 0) {
        result.code = 105;
        result.data = "no params";
    }else{
        result.data = await USER_DB.query({
            username: ctx.request.query.username
        });
    }
    ctx.response.body = result;
});

router.post('/user/add', async (ctx, next) => {
    let result = {
        code: 0,
        data: null,
    };
    result.data = await USER_DB.save(ctx.request.query);
    result.message = '用户信息生成成功'
    ctx.response.body = result;
});

router.post('/user/delete', async (ctx, next) => {
    let result = {
        code: 0,
        data: null,
    };
    result.data = await USER_DB.delete(ctx.request.query);
    result.message = '用户信息删除成功'
    ctx.response.body = result;
});

module.exports = router;