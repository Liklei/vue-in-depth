/** 
 * @func product-route
 * @author Lance
 */
const Router = require('koa-router');

// 路由前缀
const router = new Router()

router.get('/product', async (ctx, next) => {
        const result = {
            code: 200,
            data: {
                product: 1234
            },
        }
        ctx.response.body = result
})

router.get('/product/detail', async (ctx, next) => {
    const result={
        code: 200,
        data: ctx.request.query
    }
    ctx.response.body = result
})

router.get('/product/json', async (ctx, next) => {
    const result = {
        code: 200,
        data: ctx.request.query
    }
    ctx.response.body = result
})

module.exports = router
