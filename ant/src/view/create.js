var officegen = require('officegen');
var fs = require('fs');
var path = require('path');
var docx = officegen('docx');
var async = require('async');

var header = docx.getHeader().createP({
  align: ('left')
});

console.log(__dirname);

header.addText('公司名称', {
  font_size: 8,
  font_face: 'SimSun'
});
header.addHorizontalLine();

var pObj = docx.createP();
pObj.options.align = 'center';
pObj.addText("样品检测流程卡", {
  font_size: 20,
  font_face: 'KaiTi_GB2312'
});
pObj.addText('Simple');

pObj.addText(' with color', {
  color: '000088'
});

pObj.addText(' and back color.', {
  color: '00ffff',
  back: '000088'
});

pObj.addText('Bold + underline', {
  bold: true,
  underline: true
});

pObj.addText('Fonts face only.', {
  font_face: 'Arial'
});

pObj.addText(' Fonts face and size. ', {
  font_face: 'Arial',
  font_size: 40
});

pObj.addText('External link', {
  link: 'https://github.com'
});

// Hyperlinks to bookmarks also supported:
pObj.addText('Internal link', {
  hyperlink: 'myBookmark'
});
// ...
// Start somewhere a bookmark:
pObj.startBookmark('myBookmark');
// ...
// You MUST close your bookmark:
pObj.endBookmark();

var out = fs.createWriteStream('out.docx'); // 文件写入
out.on('error', function (err) {
  console.log(err);
});
var result = docx.generate(out); // 服务端生成word
