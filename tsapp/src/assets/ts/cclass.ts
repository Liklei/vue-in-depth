class Animal {
  public name: string;
  constructor(message: string) {
    this.name = message;
  }
  move(distance: number) {
    console.log(`my name is ${this.name},  move ${distance} m`);
  }
}

// class Dog extends Animal {
//   bark() {
//     console.log('Dog whoo whoo');
//   }
// }
