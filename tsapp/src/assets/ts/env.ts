
interface BaseUrlRule {
  baseUrl: string;
  routerMode?: string;
  imageBaseUrl?: string;
  env?: string;
}

let baseData: BaseUrlRule;

if (process.env.NODE_ENV === 'development') {
  baseData = {
    baseUrl: 'https://user.api.hudunsoft.com',
    routerMode: 'hash',
    imageBaseUrl: '',
    env: 'development',
  };
}
if (process.env.NODE_ENV === 'production') {
  baseData = {
    baseUrl: 'https://user.api.hudunsoft.com',
    routerMode: 'hash',
    imageBaseUrl: '',
  };
}

export {
  baseData,
};
